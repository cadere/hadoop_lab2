package dsbda;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.io.InputStreamReader;

import java.util.*;


public class Main {
    public static void  main (String[] args) throws Exception{

        if (args.length < 1) {
            System.out.println("Please, specify output filename in hdfs");
            return;
        }
        String fileUrl = args[0];
        // задаем конфигурацию контекста
        final SparkConf sparkConf = new SparkConf().setAppName("CountPriority").setMaster("local");
        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);


        //если второй аргумент uuid , генерируем рандомый uuid для чтения сообщений из кафки
        InputStreamReader inStream = new InputStreamReader(System.in);
        JavaPairRDD<String, Integer> counts = CountByHour(javaSparkContext,Consumer.consumer(inStream));
        counts.saveAsTextFile(fileUrl);
        

    }

    public static JavaPairRDD<String, Integer> CountByHour(JavaSparkContext javaSparkContext, ArrayList<String> logs){


          JavaRDD<String> lines = javaSparkContext.parallelize(logs);

        //JavaPairRDD<String,Integer> counts = 
        JavaPairRDD<String, Integer> maped_lines = lines.mapToPair((new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) {
                return new Tuple2<String, Integer>(s,1);}
            }));
        JavaPairRDD<String, Integer> counts =  maped_lines.reduceByKey((new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer i1, Integer i2) {return i1+i2; }
        }));
        
        // show
        System.out.format("|%10s|%10s|%10s|\n", "Hour", "Priority", "Count");
        counts.foreach(new VoidFunction<Tuple2<String, Integer>>() {
            @Override
            public void call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                String[] splitted = stringIntegerTuple2._1.split("\\t");

                String hour = splitted[0];
                String prioritiy = splitted[1];
                Integer count = stringIntegerTuple2._2;
                System.out.format("|%10s|%10s|%10d|\n", hour, prioritiy, count);
            }
        });

        return counts;

    }

}
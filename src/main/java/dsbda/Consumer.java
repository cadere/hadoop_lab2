package dsbda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Consumer {
    private static Pattern logLinePattern = Pattern.compile("<(\\d)>,(\\w+ \\d+ \\d\\d):",Pattern.CASE_INSENSITIVE);
    public static ArrayList<String> consumer(InputStreamReader inStream) throws IOException {
        ArrayList<String> logLines = new ArrayList<>();
        try (BufferedReader logReader = new BufferedReader(inStream)) {
            String logline = logReader.readLine();
            while (logline != null && !logline.isEmpty()) { 
                Matcher matcher = Consumer.logLinePattern.matcher(logline);
                matcher.find();
                
                String priority = matcher.group(1);
                String hour = matcher.group(2);

                logLines.add(String.format("%s\t%s", hour, priority));
                logline = logReader.readLine();
            }
  
        }
        return logLines;
    }

}
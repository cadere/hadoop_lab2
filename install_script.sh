#git clone 
#cd xxx
mvn clean install
CONTAINER_NAME=spark
docker pull sequenceiq/hadoop-docker:2.7.0
docker run --name $CONTAINER_NAME -p 50070:50070  -it sequenceiq/hadoop-docker:2.7.0 /etc/bootstrap.sh -bash
sudo docker start $CONTAINER_NAME
docker cp target/logPrior-1.0-SNAPSHOT.jar $CONTAINER_NAME:/opt/
docker cp kern.log $CONTAINER_NAME:/opt/
docker cp spark $CONTAINER_NAME:/usr/local/

sudo docker start $CONTAINER_NAME
docker exec -it $CONTAINER_NAME bash

SPARK_PATH=/usr/local/spark/bin
hdfs_url=`( $HADOOP_PREFIX/bin/hdfs getconf -confkey fs.defaultFS )`
$HADOOP_PREFIX/bin/hdfs dfs -mkdir $hdfs_url/input
$HADOOP_PREFIX/bin/hdfs dfs -put /opt/kern.log $hdfs_urlinput/kern.log
$HADOOP_PREFIX/bin/hdfs dfs -mkdir $hdfs_url/output

$HADOOP_PREFIX/bin/hdfs dfs -cat $hdfs_url/input/kern.log
$SPARK_PATH/spark-submit --class dsbda.Main /opt/logPrior-1.0-SNAPSHOT.jar $hdfs_url/output
$HADOOP_PREFIX/bin/hdfs dfs -cat $hdfs_url/output/part*
